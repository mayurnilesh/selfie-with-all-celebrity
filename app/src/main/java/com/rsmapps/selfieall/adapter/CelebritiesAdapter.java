package com.rsmapps.selfieall.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.DrawableTransformation;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.rsmapps.selfieall.R;
import com.rsmapps.selfieall.activity.KohaliActivity;
import com.rsmapps.selfieall.fragment.CelebrityImagesFragment;
import com.rsmapps.selfieall.model.Celebrity;

import org.w3c.dom.Text;

import java.util.ArrayList;


/**
 * Created by Lincoln on 31/03/16.
 */

public class CelebritiesAdapter extends RecyclerView.Adapter<CelebritiesAdapter.MyViewHolder> {
    private ArrayList<Celebrity> mList = new ArrayList<>();
    private Activity mContext;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textView,textCount;
        public ImageView icon;


        public MyViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.title);
            icon = view.findViewById(R.id.icon);
            textCount = view.findViewById(R.id.titleCount);
            //icon.setVisibility(View.GONE);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            CelebrityImagesFragment fragment = CelebrityImagesFragment.newInstance(mList.get(getAdapterPosition()),"");
            ((KohaliActivity)mContext).replaceFragment(fragment,CelebrityImagesFragment.class.getSimpleName(),true,false);
        }
    }

    public CelebritiesAdapter(Activity context, ArrayList<Celebrity> list) {
        mContext = context;
        this.mList =  list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.celebrity_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.textView.setText(mList.get(position).getName() );

        String imagePath="";
        if( mList.get(position).getImages() != null && mList.get(position).getImages().size() > 0 && mList.get(position).getImages().get(0) != null ){
            holder.textCount.setText("("+String.valueOf(mList.get(position).getImages().size())+")");

            imagePath =mList.get(position).getImages().get(0);
        }else{
            holder.textCount.setText("");
            imagePath="";
        }

        /*RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));*/

        Glide.with(mContext).load(imagePath)
                .apply(new RequestOptions().placeholder(R.drawable.actor_placeholder).error(R.drawable.actor_placeholder))
                .transform(new CenterInside(), new RoundedCorners(16))
                .into(holder.icon);


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}