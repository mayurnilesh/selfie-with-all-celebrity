package com.rsmapps.selfieall.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.aligohershabir.photocollage.model.Path;
import com.aligohershabir.photocollage.utils.ResourceManager;
import com.aligohershabir.photocollage.view.activity.CollageActivity;
import com.google.android.gms.ads.AdListener;
import com.instapic.activities.InstaPicActivity;
import com.mirror.activities.MirrorActivity;
import com.mirror.utils.Constant;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.rsmapps.selfieall.AdsUtil;
import com.rsmapps.selfieall.R;
import com.rsmapps.selfieall.databinding.ActivityHomeBinding;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    enum SelectedMenu {
        COLLAGE, MIRROR, PIP
    }

    public static final String TAG = HomeActivity.class.getSimpleName();

    private static final int REQUEST_READ_PERMISSION = 101;
    private static final int REQUEST_PICK_MIRROR_IMAGE_EFFECT = 102;
    private static final int REQUEST_PICK_PIP = 103;
    private static final int REQUEST_SAVE_REDIRECTION = 104;

    private static SelectedMenu selectionType = SelectedMenu.COLLAGE;


    public static final String APP_URL =
            "https://play.google.com/store/apps/details?id=" + com.rsmapps.selfieall.helper.ResourceManager.PACKAGE_NAME;

    public static final String APP_URL_PHOTO_MOVIE_MAKER =
            "https://play.google.com/store/apps/details?id=com.rsmapps.photovideomaker";

    private ActivityHomeBinding binding;

    private void setInfo() {
//        AdsUtil.showBannerAd(this, (AdView) findViewById(R.id.banner_adview_bottom));


        binding.llSavePhoto.setOnClickListener(this);
        //binding.ivDownload.setOnClickListener(this);
        binding.llRate.setOnClickListener(this);
        //findViewById(R.id.llRate).setOnClickListener(this);
        //findViewById(R.id.llSavePhoto).setOnClickListener(this);


        //binding.cvHomeAd.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * New Controls
             */
            case R.id.ivSelfie:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        startActivity(new Intent(HomeActivity.this, KohaliActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);

                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdLoaded();
                        startActivity(new Intent(HomeActivity.this, KohaliActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.ivCollage:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        selectionType = SelectedMenu.COLLAGE;
                        requestStoragePermission();
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        selectionType = SelectedMenu.COLLAGE;
                        requestStoragePermission();
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.ivMirror:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        selectionType = SelectedMenu.MIRROR;
                        requestStoragePermission();
                        /*Intent mirror = new Intent(HomeActivity.this, com.mirror.activities.PhotoSelectActivity.class);
                        mirror.putExtra("MIRROR","mirror");
                        mirror.putExtra(Constant.FEATURE,Constant.PIP);
                        startActivity(mirror);*/
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        selectionType = SelectedMenu.MIRROR;
                        requestStoragePermission();
                        /*Intent mirror = new Intent(HomeActivity.this, com.mirror.activities.PhotoSelectActivity.class);
                        mirror.putExtra("MIRROR","mirror");
                        mirror.putExtra(Constant.FEATURE,Constant.PIP);
                        startActivity(mirror);*/
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.ivInstaPic:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        selectionType = SelectedMenu.PIP;
                        requestStoragePermission();
                        /*Intent pip = new Intent(HomeActivity.this, com.instapic.activities.PhotoSelectActivity.class);
                        pip.putExtra("SHAPE","shape");
                        pip.putExtra(Constant.FEATURE, Constant.PIP);
                        startActivity(pip);*/
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        selectionType = SelectedMenu.PIP;
                        requestStoragePermission();
                        /*Intent pip = new Intent(HomeActivity.this, com.instapic.activities.PhotoSelectActivity.class);
                        pip.putExtra("SHAPE","shape");
                        pip.putExtra(Constant.FEATURE, Constant.PIP);
                        startActivity(pip);*/
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.llSavePhoto:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        startActivity(new Intent(HomeActivity.this, com.savedPhotos.activity.MyCreationActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);

                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdLoaded();
                        startActivity(new Intent(HomeActivity.this,
                                com.savedPhotos.activity.MyCreationActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.llRate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_URL)));
                break;
            /*case R.id.ivDownload:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_URL_PHOTO_MOVIE_MAKER)));
                break;*/
//            case R.id.cvHomeAd:
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_URL_PHOTO_MOVIE_MAKER)));
//                break;

           /* *//**
             * Old Controls
             *//*
            case R.id.ivStartNow:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        startActivity(new Intent(HomeActivity.this, KohaliActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);

                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdLoaded();
                        startActivity(new Intent(HomeActivity.this, KohaliActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;
            case R.id.ivPhotoCollageEditor:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        requestStoragePermission();
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        requestStoragePermission();
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;*/
            /*case R.id.rlSavePhoto:
                AdsUtil.showInterstitialAd(HomeActivity.this, new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        startActivity(new Intent(HomeActivity.this,
                                com.savedPhotos.activity.MyCreationActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);

                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdLoaded();
                        startActivity(new Intent(HomeActivity.this,
                                com.savedPhotos.activity.MyCreationActivity.class));
                        AdsUtil.preloadInterstitialAd(HomeActivity.this);
                    }
                });
                break;*/
           /* case R.id.rlRate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_URL)));
                break;*/
        }
    }

    private void requestStoragePermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_PERMISSION);
        } else {
            startImagePickerActivity();
        }

    }

    private void startImagePickerActivity() {
        if (selectionType == SelectedMenu.COLLAGE) {
            Intent mIntent = new Intent(this, PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 9);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
        } else if (selectionType == SelectedMenu.MIRROR) {

            Intent mIntent = new Intent(this, PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_MIRROR_IMAGE_EFFECT);

            /*Intent select = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(select, "Select Picture"), REQUEST_PICK_MIRROR_IMAGE_EFFECT);*/
        } else if (selectionType == SelectedMenu.PIP) {

            Intent mIntent = new Intent(this, PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_PIP);

            /*Intent select = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(select, "Select Picture"), REQUEST_PICK_PIP);*/
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startImagePickerActivity();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
//            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
//            // do your logic here...
//            ResourceManager.images = images;
//            startActivity(new Intent(ChooserActivity.this, CollageActivity.class));
//        }
        //super.onActivityResult(requestCode, resultCode, data);  // You MUST have this line to be here
        // so ImagePicker can work with fragment mode

        if (resultCode != RESULT_OK) {
            return;
        }

        ArrayList<String> pathList;
        if (requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < pathList.size(); i++) {
                    sb.append("Photo" + (i + 1) + ":" + pathList.get(i));
                    sb.append("\n");
                }
//                Log.i(TAG, "pathList: " + sb.toString());
            }
            ArrayList<Path> paths = new ArrayList<>();
            for (String s : pathList) {
                paths.add(new Path(s, false));
            }
            ResourceManager.paths = paths;
            startActivityForResult(new Intent(HomeActivity.this, CollageActivity.class),REQUEST_SAVE_REDIRECTION);
        } else if (requestCode == REQUEST_PICK_MIRROR_IMAGE_EFFECT) {

                 pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
                if (pathList != null && !pathList.isEmpty()) {
                    try {
                        Uri uri = Uri.fromFile(new File(pathList.get(0)));
                        getBitmapFromUri(uri);
                        Intent intent = new Intent(HomeActivity.this, MirrorActivity.class);
                        startActivity(intent);
                    } catch (Exception e) {
                        return;
                    }
                }


                /*Uri uri = data.getData();
                getBitmapFromUri(uri);
                Intent intent = new Intent(HomeActivity.this, MirrorActivity.class);
                startActivity(intent);*/


        } else if (requestCode == REQUEST_PICK_PIP) {


                pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
                if (pathList != null && !pathList.isEmpty()) {
                    try {
                        Uri uri = Uri.fromFile(new File(pathList.get(0)));
                        getBitmapFromUri(uri);
                        Intent intent = new Intent(HomeActivity.this, InstaPicActivity.class);

                        intent.putExtra("SHAPE","shape");
                        intent.putExtra(Constant.FEATURE, Constant.PIP);

                        startActivity(intent);
                    } catch (Exception e) {
                        return;
                    }
                }




                /*Uri uri = data.getData();
                getBitmapFromUri(uri);
                Intent intent = new Intent(HomeActivity.this, InstaPicActivity.class);

                intent.putExtra("SHAPE","shape");
                intent.putExtra(Constant.FEATURE, Constant.PIP);

                *//*if (getIntent().hasExtra("url")) {
                    intent.putExtra("url", getIntent().getStringExtra("url"));
                }*//*

                startActivity(intent);*/

        }else if(requestCode == REQUEST_SAVE_REDIRECTION){
            Intent savedActivityIntent = new Intent(HomeActivity.this, com.savedPhotos.activity.MyCreationActivity.class);
            savedActivityIntent.putExtra(com.savedPhotos.activity.MyCreationActivity.FROM, TAG);
            startActivity(savedActivityIntent);
        }

    }

    private void getBitmapFromUri(Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            if (bitmap != null)
                com.rsmapps.selfieall.helper.ResourceManager.bitmap = bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
